const TokenController = require('./controller/token');
const MeController = require('./controller/me');

const createRoute = require('./helper/createRoute');

module.exports = api => {
  const route = createRoute(api);

  route('/token', TokenController);
  route('/me', MeController);
};
