const bcrypt = require('bcrypt');
const response = require('../helper/response');
const model = require('../model');
const jwt = require('jsonwebtoken');
const config = require('../config');

const { Users } = model;

const schema = {
  token: String,
};

const TokenController = {
  post(req) {
    const { email, password } = req.body;

    return Users.findOne({
      where: { email },
    })
      .then(data => {
        if (!data) {
          return response.error('User not found', 404);
        }

        return bcrypt
          .compare(password, data.password)
          .then(match => [data, match]);
      })
      .then(([data, match]) => {
        if (!match) {
          return response.error('Password is not match', 400);
        }

        const token = jwt.sign({ email: data.email }, config.JWT_SECRET);

        return response.single({ token }, schema);
      });
  },
};

module.exports = TokenController;
