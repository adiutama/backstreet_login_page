const bcrypt = require('bcrypt');
const response = require('../helper/response');
const model = require('../model');
const jwt = require('jsonwebtoken');
const config = require('../config');

const { Users } = model;

const schema = {
  email: String,
  createdAt: Date,
  updatedAt: Date,
};

const MeController = {
  all(req) {
    const exclude = ['POST'];
    const token = req.headers.authorization;

    if (exclude.indexOf(req.method) < 0) {
      jwt.verify(token, config.JWT_SECRET);
    }
  },

  get(req) {
    const data = jwt.decode(req.headers.authorization, config.JWT_SECRET);

    return response.single(data, schema);
  },

  post(req) {
    const { email, password } = req.body;

    return bcrypt
      .hash(password, 10)
      .then(hash => {
        return Users.findOrCreate({
          where: {
            email,
          },
          defaults: {
            password: hash,
          },
        });
      })
      .then(([data, created]) => {
        if (created) {
          return response.single(data, schema, 201);
        } else {
          return response.error('User is already exists', 409);
        }
      });
  },
};

module.exports = MeController;
