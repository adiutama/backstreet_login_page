/** ===== Modules ===== **/
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const route = require('./route');

/** ===== Middleware ===== **/
const api = express.Router();

api.use(bodyParser.urlencoded({ extended: false }));
api.use(bodyParser.json());
api.use(cors());

route(api);

module.exports = api;
