module.exports = app => (path, resource) => {
  const route = app.route(path);

  Object.keys(resource).forEach(verb => {
    route[verb]((req, res, next) => {
      const method = resource[verb];

      Promise.resolve()
        .then(() => {
          return method(req);
        })
        .then(response => {
          if (verb == 'all') {
            next();
          } else {
            res.status(response.status).send(response);
          }
        })
        .catch(err => {
          const status = err.status || 500;
          const error = err.message;

          res.status(status).send({
            status,
            error,
          });
        });
    });
  });
};
