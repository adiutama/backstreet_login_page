const schema = require('schm');

class ResourceError extends Error {
  constructor(message, status) {
    super(message);

    this.status = status;
  }
}

const response = {
  single(model, rule, status = 200) {
    const result = schema(rule).parse(model);

    return { status, result };
  },

  error(message, status = 500) {
    throw new ResourceError(message, status);
  },
};

module.exports = response;
