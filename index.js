/** ===== Modules ===== **/
const express = require('express');
const api = require('./api');

/** ===== Config ===== **/
const PORT = process.env.PORT || 8080;

/** ===== Middleware ===== **/
const app = express();

/** ===== Router ===== **/
app.use('/', express.static(__dirname + '/client/dist/'));
app.use('/api', api);

/** ===== Server ===== **/
app.listen(PORT, () => {
  console.log('Server is running');
});
