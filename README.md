# Backstreet Academy Technical Assessment

## Getting Started

Before starting the app first you must build the client. To do
that you must go to client directory then make sure install module before build.

```
$ cd ./client
$ npm install
$ npm run build
```

After start the app server don't forget to go back to parent folder and
make sure you've install the required module before starting.

```
$ cd ..
$ npm install
$ npm run start
```

Now you can access the app from http://localhost:8080.
